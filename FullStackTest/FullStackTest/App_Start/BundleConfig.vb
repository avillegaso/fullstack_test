﻿Imports System.Web
Imports System.Web.Optimization

Public Module BundleConfig
    ' For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
    Public Sub RegisterBundles(ByVal bundles As BundleCollection)

        'Plugins Template
        bundles.Add(New ScriptBundle("~/Template/js").Include(
            "~/Scripts/jquery/jquery-1.12.4.min.js",
            "~/Scripts/jquery/jquery.ui.touch-punch.min.js",
            "~/Scripts/bootstrap/bootstrap.min.js",
            "~/Scripts/jquery/jquery.slimscroll.min.js",
            "~/Scripts/fastclick/fastclick.js",
            "~/Scripts/demo/demo.js",
            "~/Scripts/template/adminlte.js"))

        'Validaciones
        bundles.Add(New ScriptBundle("~/bundles/jqueryval").Include(
            "~/Scripts/jquery/jquery.validate*"))

        ' Use the development version of Modernizr to develop with and learn from. Then, when you're
        ' ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
        bundles.Add(New ScriptBundle("~/bundles/modernizr").Include(
            "~/Scripts/modernizr/modernizr-{version}.js"))

        'Libs
        bundles.Add(New ScriptBundle("~/bundles/libs").Include(
            "~/Scripts/moment/moment.js"))

        'Servicios AngularJs
        bundles.Add(New ScriptBundle("~/bundles/angular").Include(
            "~/Scripts/angular/angular.js",
            "~/Scripts/angular/angular-animate.js",
            "~/Scripts/angular-ui/angular-ui.js",
            "~/Scripts/angular-datetimepicker/datetimeinput.js",
            "~/Scripts/angular-datetimepicker/datetimepicker.js",
            "~/Scripts/angular-datetimepicker/datetimepicker.templates.js",
            "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
            "~/Scripts/angular-ui/ui-bootstrap.js",
            "~/Scripts/angular-toastr/toaster.js"))

        'Plugins
        bundles.Add(New ScriptBundle("~/bundles/site").Include(
            "~/Scripts/alertifyjs/alertify.js",
            "~/Scripts/angular-toastr/toaster.min.js",
            "~/Scripts/linq/linq.min.js",
            "~/Scripts/pagination/dirPagination.js"))

        'Configuración Aplicación
        bundles.Add(New ScriptBundle("~/bundles/app").Include(
            "~/App/site.js",
            "~/App/app.js",
            "~/App/controllers/studentCtrl.js"))

        'Estilos Template
        bundles.Add(New StyleBundle("~/Template/css").Include(
            "~/Content/bootstrap.min.css",
            "~/Content/font-awesome.min.css",
            "~/Content/ionicons.min.css",
            "~/Content/AdminLTE.css",
            "~/Content/_all-skins.min.css"
        ))

        'Estilos Css
        bundles.Add(New StyleBundle("~/Content/css").Include(
            "~/Content/Site.css",
            "~/Content/angular-ui.css",
            "~/Content/datetimepicker.css",
            "~/Content/alertify.css",
            "~/Content/default.css",
            "~/Content/animate.css",
            "~/Content/toaster.min.css",
            "~/Content/jquery-ui.css"
))

        BundleTable.EnableOptimizations = False

    End Sub
End Module
