﻿app.controller('studentCtrl', ['$scope', '$http', '$timeout', '$filter', '$q', 'toaster', function ($scope, $http, $timeout, $filter, $q, toaster) {

    $scope.studentList = [];
    $scope.student = {};
    $scope.studentTmp = {};
    $scope.search = [];
    $scope.validate = false;
    $scope.mode = false;

    $scope.initCtr = function () {
        try {
            var req = {
                method: 'POST',
                url: 'api/Student/GetStudentList',
                headers: {
                    'Content-Type': 'aplication/json'
                }
            }
            $http(req).then(function success(response) {
                if (response.data.students.length > 0) {
                    angular.copy(response.data.students, $scope.studentList);
                }
            }, function error(response) {
                    toaster.pop('error', 'Student', response);
            });
        } catch (e) {
            console.log(e);
        }
    };

    $scope.edit = function (student, mode) {
        try {
            removeRequired();
            $scope.studentTmp = {};
            $scope.mode = mode;
            angular.copy(student, $scope.studentTmp)
            $scope.student = student;
            if (IsDefinedAndNotEmpty($scope.student)) {
                $scope.validate = true;
            }
            $('#studentModal').modal('show');
        } catch (e) {
            console.log(e);
        }
    };

    $scope.validateUsername = function (username) {
        try {
            if (IsDefinedAndNotEmpty(username)) {
                var req = {
                    method: 'POST',
                    url: 'api/Student/ExistUsername',
                    data: { username: username }
                }
                $http(req).then(function success(response) {
                    if (!response.data.exist) {
                        $scope.validate = true;
                        toaster.pop('success', 'Student', 'The username is valid.');
                    } else {
                        toaster.pop('error', 'Student', 'The username already exist.');
                    }
                }, function error(response) {
                    toaster.pop('error', 'Student', response);
                    $scope.validate = false;
                });
            }
        } catch (e) {
            console.log(e);
        }
    };

    $scope.createStudent = function () {
        try {
            if (!$scope.validar()) {
                var req = {
                    method: 'POST',
                    url: 'api/Student/CreateStudent',
                    data: { student: JSON.stringify($scope.student), mode: $scope.mode }
                }
                $http(req).then(function success(response) {
                    if (response.data.error)
                        toaster.pop({
                            type: 'error', title: 'Save student', body: response.data.status
                        });
                    else {
                        var tmpStudentList = $scope.studentList;
                        $scope.studentList = [];
                        $scope.studentList = ActualizarTabla(tmpStudentList, $scope.student, response.data.id);
                        tmpStudentList = [];

                        $scope.student = {};
                        toaster.pop({
                            type: 'success', title: 'Save student', body: 'Succesfully.'
                        });

                        $('#studentModal').modal('hide');
                        $scope.$apply();
                    }
                }, function error(response) {
                    toaster.pop('error', 'Student', response);
                });
            }
        } catch (e) {
            console.log(e);
        }
    };

    $scope.validar = function () {
        removeRequired();
        var error = false;
        if (!IsDefinedAndNotEmpty($scope.student.username)) {
            $('.username').addClass('required');
            error = true;
        }
        if (!IsDefinedAndNotEmpty($scope.student.firstName)) {
            $('.firstName').addClass('required');
            error = true;
        }
        if (!IsDefinedAndNotEmpty($scope.student.lastName)) {
            $('.lastName').addClass('required');
            error = true;
        }
        if (!IsDefinedAndNotEmpty($scope.student.age)) {
            $('.age').addClass('required');
            error = true;
        }
        if (!IsDefinedAndNotEmpty($scope.student.career)) {
            $('.career').addClass('required');
            error = true;
        }
        
        if (error) {
            toaster.pop('error', 'Student', 'Fill out the entire form.'
            );
        }
        return error;
    };

    $scope.delete = function (id) {
        try {
            ConfirmarMensaje('¿Do you really want to delete the record?', function (result) {
                if (result) {

                    var req = {
                        method: 'POST',
                        url: 'api/Student/Delete',
                        data: { id: id }
                    }
                    $http(req).then(function success(response) {
                        if (response.data.error) {
                            toaster.pop('error', 'Delete', response.data.status);
                        } else {
                            toaster.pop('success', 'Delete', 'succesfully.');
                            var tmpStudentList = $scope.studentList;
                            $scope.studentList = [];
                            $scope.studentList = EliminarRegistroTabla(tmpStudentList, id);
                            studentList = [];
                        }
                    }, function error(response) {
                        toaster.pop('error', 'Student', response);
                    });
                }
            });
        } catch (e) {
            console.log(e);
        }
    };

    $scope.initCtr();
}]);