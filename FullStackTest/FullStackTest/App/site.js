﻿function removeRequired() {
    $('input').each(function () {
        if ($(this).hasClass('required'))
            $(this).removeClass('required');
    });
    $('select').each(function () {
        if ($(this).hasClass('required'))
            $(this).removeClass('required');
    });
    $('select').each(function () {
        if ($(this).parent().find('.select2').hasClass('required'))
            $(this).parent().find('.select2').removeClass('required');
    });
    $('textarea').each(function () {
        if ($(this).hasClass('required'))
            $(this).removeClass('required');
    });
    $('datetimepicker').each(function () {
        if ($(this).hasClass('required'))
            $(this).removeClass('required');
    });

}


/**
 * retorna true o false dependiendo de si es vacía o null
 */
function IsNullOrEmpty(pStringVar) {

    var isNull = false;
    if (pStringVar == null) {
        isNull = true;
    } else if (pStringVar === "") {
        isNull = true;
    } else if (pStringVar.length == 0) {
        isNull = true;
    } else if (pStringVar.length <= 0) {
        isNull = true;
    }

    return isNull;
}

function IsUndefined(pVar) {

    var isUndefined = false;
    if (typeof (pVar) == "undefined") {
        isUndefined = true;
    }

    return isUndefined;
}

function IsDefined(pVar) {
    return typeof pVar !== 'undefined';
}

function IsDate(StringDate) {
    return ((new Date(StringDate) !== "Invalid Date" && !isNaN(new Date(StringDate))));
}

/**
 * [Valida si una variable esta definida y contiene datos]
 */
function IsDefinedAndNotEmpty(pVar, tipoDato, campo, valRetornoCaseFalse) {
    var valueReturn;
    if (IsDefined(tipoDato)) {
        if (IsDefined(pVar) && !IsNullOrEmpty(pVar)) {
            if (IsDefined(campo)) {
                if (typeof pVar == 'object') {
                    valueReturn = pVar[campo];
                } else {
                    valueReturn = pVar;
                }
            } else {
                valueReturn = pVar;
            }

        } else {
            if (IsDefined(valRetornoCaseFalse)) {
                return valRetornoCaseFalse;
            } else {

                switch (tipoDato.toLowerCase()) {
                    case "string": valueReturn = "";
                        break;
                    case "array": valueReturn = [];
                        break;
                    case "object": valueReturn = {};
                        break;
                    case "number": valueReturn = 0;
                        break;
                    case "date": valueReturn = {};
                        break;
                    default: valueReturn = "";
                }
            }
        }
    } else {
        if (typeof pVar == 'object') {
            valueReturn = false;

            if (pVar) {
                if (Object.keys(pVar).length > 0) {
                    valueReturn = true;
                } else {
                    if (IsDate(pVar)) {
                        valueReturn = true;
                    }
                }
            }
        } else {
            valueReturn = IsDefined(pVar) && !IsNullOrEmpty(pVar);
        }
    }
    return valueReturn;
}
function ConfirmarMensaje(mensaje, callBack) {

    alertify.confirm("Full Stack Test", mensaje, function () {
        callBack(true);
    },
    function () {
        callBack(false);
    });

}

/*
* Retorna la lista actualizada según 
* Si es adición realiza un push a la lista,
* Si es actualización busca el id y actualiza el objeto
* Espera por parametro la lista principal, el Objeto Agregado el Id que 
* retorna la data en el metodo
*/
function ActualizarTabla(lista, obj, id) {
    try {
        if (IsDefinedAndNotEmpty(lista) && IsDefinedAndNotEmpty(obj)) {
            var indiceEnLista = lista.findIndex(function (m) { return m.Id == id; });
            if (indiceEnLista != -1) {
                lista[indiceEnLista] = obj;
            } else {
                if (IsDefinedAndNotEmpty(obj.$$hashKey)) {
                    delete obj.$$hashKey;
                }
                obj.id = id;
                lista.push(obj);
            }
        } else {
            if (!IsDefinedAndNotEmpty(lista) && IsDefinedAndNotEmpty(obj)) {
                obj.Id = id;
                lista.push(obj);
            }
        }
    } catch (e) {
        console.log();
    }
    return OrdenarListaDescendientePorIds(lista);
};

/*
* Retorna la lista ordenada luego de ser eliminado 
* el item de la lista
* Recibe por parametro la lista que se va a 
* actualizar y el id
*/
function EliminarRegistroTabla(lista, id) {
    try {
        if (IsDefinedAndNotEmpty(lista) && id > 0) {
            var indiceEnLista = lista.findIndex(function (m) { return m.Id == id; });
            if (indiceEnLista != -1) {
                lista.splice(indiceEnLista, 1);
            }
        }
    } catch (e) {
        console.log(e);
    }
    return OrdenarListaDescendientePorIds(lista);
};

/*
* Por medio de linq reordena las posiciones
* por id descendiente.
* Espera por parametro la lista a ordenar
*/
function OrdenarListaDescendientePorIds(lista) {
    try {
        if (IsDefinedAndNotEmpty(lista)) {
            var listaOrdenada = [];
            listaOrdenada = Enumerable
				.From(lista)
				.Select(function (m) { return m; })
				.OrderByDescending(function (o) { return o.Id; })
				.ToArray();

            return listaOrdenada;
        }

    } catch (e) {
        console.log(e);
    }
}

