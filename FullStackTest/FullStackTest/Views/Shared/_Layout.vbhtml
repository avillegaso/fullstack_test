﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>@ViewBag.Title</title>
    @Styles.Render("~/Template/css")
	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	@Styles.Render("~/Content/css")
    @Scripts.Render("~/bundles/modernizr")
  
</head>
<body class="hold-transition skin-black sidebar-collapse" >
    <div class="box box-solid full-page">
        <div class="overlay hidden" id="waittingModal">
            <i class="fa fa-spinner fa-pulse fa-2x"></i>
            <div class="centerScreen">
                <span id="waittingModalText"></span>
            </div>
        </div>
        <div class="wrapper" ng-app="appOrbit">
            <toaster-container></toaster-container>
            <!--Carga la cabecera del Template-->
            @Html.Partial("_Header")
            <!--Carga el contenido de la pagina-->
            @RenderBody()

            <!--Carga el footer de la aplicación-->
            @Html.Partial("_Footer")
        </div>
    </div>
        @Scripts.Render("~/Template/js")
        @Scripts.Render("~/bundles/libs")
        @Scripts.Render("~/bundles/angular")
        @Scripts.Render("~/bundles/app")
        @Scripts.Render("~/bundles/site")
        @RenderSection("scripts", required:=False)
</body>
</html>
