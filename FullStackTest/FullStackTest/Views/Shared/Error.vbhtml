﻿<div class="content-wrapper">
    <!-- Cabecera de la pagina -->
    <section class="content-header">
        <h1>Error!</h1>
    </section>
    <!-- Contenedor -->
    <section class="content hold-transition animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <br />
                        <div class="row">
                            <div class="error-page">
                                <div class="error-content">
                                    <h3><i class="fa fa-warning text-red"></i> Oops! Usuario no encontrado.</h3>
                                    <p>
                                        Cominiquese con el administrador.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Fin Contenedor -->
</div>
