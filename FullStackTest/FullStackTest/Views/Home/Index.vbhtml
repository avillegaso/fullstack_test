﻿<div ng-controller="studentCtrl">
    @Html.Partial("_StudentModal")
    <div class="content-wrapper">
        <section class="content hold-transition animated fadeInRight">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="group-btn">
                                <button class="btn btn-flat btn-primary btn-social ng-binding" ng-click="edit({}, true)" id="BtnAgregar">
                                    <i class="fa fa-plus"></i>New
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">
                                        Student list
                                    </h3>
                                    <br />
                                    <div class="box-tools pull-right">
                                        <div class="input-group input-group-sm" style="width: 250px;">
                                            <input type="text" name="table_search" class="form-control pull-right" ng-model="search" placeholder="Buscar en Tabla">
                                            <div class="input-group-btn">
                                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive table-height">
                                            <table class="table table-striped table-bordered tables">
                                                <thead class="cabecera-Tabla">
                                                    <tr class="header-table">
                                                        <td class="text-center" width="5px">Edit</td>
                                                        <td class="text-center" width="5px">Delete</td>
                                                        <td class="text-center"><span>Username</span></td>
                                                        <td class="text-center"><span>Firstname</span></td>
                                                        <td class="text-center"><span>Lastname</span></td>
                                                        <td class="text-center"><span>Age</span></td>
                                                        <td class="text-center"><span>Career</span></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr dir-paginate="student in studentList | filter:search | itemsPerPage:7 ">
                                                        <td align="center">
                                                            <span role="button" ng-click="edit(student,false);">
                                                                <i class="fa fa-2x fa-edit"></i>
                                                            </span>
                                                        </td>
                                                        <td align="center">
                                                            <span role="button" ng-click="delete(student.id);"><i class="fa fa-2x fa-trash"></i></span>
                                                        </td>
                                                        <td>{{student.username}}</td>
                                                        <td>{{student.firstName}}</td>
                                                        <td>{{student.lastName}}</td>
                                                        <td>{{student.age}}</td>
                                                        <td>{{student.career}}</td>
                                                </tbody>
                                            </table>
                                            <div style="text-align: center;">
                                                <dir-pagination-controls direction-links="true" boundary-links="true" class="center-box-h" ></dir-pagination-controls>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

