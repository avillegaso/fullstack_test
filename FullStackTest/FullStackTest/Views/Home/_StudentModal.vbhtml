﻿<div class="modal fade" role="dialog" id="studentModal">
    <div class="alineacionVertical">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create student</h3>
                        <div class="row">
                            <br>
                            <div class="col-md-12">
                                <div class="btn-group">
                                    <div class="btn-group-btn">
                                        <button class="btn btn-flat btn-social btn-primary" ng-click="createStudent()">
                                            Guardar<i class="fa fa-save"></i>
                                        </button>
                                        &nbsp;
                                        <button class="btn btn-flat btn-social btn-primary" data-dismiss="modal">
                                            Descartar<i class="fa fa-reply"></i>
                                        </button>
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="well">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Username</div>
                                            <input type="text" class="form-control username" aria-describedby="basic-addon3"
                                                   ng-model="student.username" ng-blur="validateUsername(student.username)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">First Name</div>
                                            <input type="text" class="form-control firstName" aria-describedby="basic-addon3"
                                                   ng-model="student.firstName" ng-disabled="!validate">
                                        </div>
                                    </div>
                                </div><div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Last Name</div>
                                            <input type="text" class="form-control lastName" aria-describedby="basic-addon3"
                                                   ng-model="student.lastName" ng-disabled="!validate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Age</div>
                                            <input type="number" class="form-control age" aria-describedby="basic-addon3"
                                                   ng-model="student.age" ng-disabled="!validate">
                                        </div>
                                    </div>
                                </div><div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Career</div>
                                            <input type="text" class="form-control career" aria-describedby="basic-addon3"
                                                   ng-model="student.career" ng-disabled="!validate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

