﻿Imports System.Data.SQLite

Public Class Data
    Private connection As String = ConfigurationManager.ConnectionStrings.Item("BBDD").ConnectionString

    Public Function GetListStudent() As List(Of Student)
        Dim studentList As New List(Of Student)
        Try
            Using conn As New SQLiteConnection(connection)
                conn.Open()
                Dim consulta As String = "select * from Student"
                Using comando As New SQLiteCommand(consulta, conn)
                    comando.CommandTimeout = Integer.MaxValue
                    Using reader = comando.ExecuteReader()
                        While reader.Read()
                            Dim c As New Student()
                            c.id = reader.GetValue(0)
                            c.username = reader.GetValue(1)
                            c.firstName = reader.GetValue(2)
                            c.lastName = reader.GetValue(3)
                            c.age = reader.GetValue(4)
                            c.career = reader.GetValue(5)
                            studentList.Add(c)
                        End While
                    End Using
                End Using
                conn.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return studentList
    End Function

    Public Function ExistUser(ByVal username As String) As Boolean
        Dim exist As Boolean = False
        Using conn As New SQLiteConnection(connection)
            conn.Open()
            Dim consulta As String = My.Resources.Queries.validateUsername
            Using comando As New SQLiteCommand(consulta, conn)
                comando.Parameters.Add(New SQLiteParameter("@Username", username))
                comando.CommandTimeout = Integer.MaxValue
                exist = comando.ExecuteScalar() > 0
            End Using
            conn.Close()
        End Using
        Return exist
    End Function

    Function Save(ByVal studentObj As Student, ByVal isNew As Boolean) As Integer
        Using conexion As New SQLiteConnection(connection)
            Dim sql As String = ""
            conexion.Open()
            Using tran = conexion.BeginTransaction()
                Try
                    If isNew Then
                        sql = My.Resources.Queries.Save
                    Else
                        sql = My.Resources.Queries.Update
                    End If
                    Using command As New SQLiteCommand(Sql, conexion, tran)
                        command.Parameters.Add(New SQLiteParameter("@Id", studentObj.id))
                        command.Parameters.Add(New SQLiteParameter("@Username", studentObj.username))
                        command.Parameters.Add(New SQLiteParameter("@FirstName", studentObj.firstName))
                        command.Parameters.Add(New SQLiteParameter("@LastName", studentObj.lastName))
                        command.Parameters.Add(New SQLiteParameter("@Age", studentObj.age))
                        command.Parameters.Add(New SQLiteParameter("@Career", studentObj.career))

                        command.CommandTimeout = Integer.MaxValue
                        Using rs = command.ExecuteReader()
                            While rs.Read()
                                studentObj.id = rs.GetValue(0)
                            End While
                        End Using
                    End Using
                    tran.Commit()
                Catch ex As Exception
                    tran.Rollback()
                    Throw ex
                End Try
            End Using
            conexion.Close()
        End Using
        Return studentObj.id
    End Function

    Public Sub Delete(ByVal id As Integer)
        Using conn As New SQLiteConnection(connection)
            conn.Open()
            Using tran = conn.BeginTransaction()
                Try
                    Dim commandText As String = My.Resources.Queries.Delete
                    Using command As New SQLiteCommand(commandText, conn, tran)
                        command.Parameters.Add(New SQLiteParameter("@Id", id))
                        command.CommandTimeout = Integer.MaxValue
                        command.ExecuteNonQuery()
                    End Using
                    tran.Commit()
                Catch ex As Exception
                    tran.Rollback()
                    Throw ex
                End Try
            End Using
            conn.Close()
        End Using
    End Sub
End Class
