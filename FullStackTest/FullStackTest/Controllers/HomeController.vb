﻿Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        ViewData("Title") = "Full Stack Test"
        ViewData("Usuario") = "Danny Alejandro Villegas Ocampo"
        Return View()
    End Function
End Class
