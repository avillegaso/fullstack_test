﻿Imports System.Net
Imports System.Net.Http
Imports System.Web.Http
Imports Newtonsoft.Json

Public Class StudentController
    Inherits ApiController
    Private bbdd As New Data()

    <HttpPost>
    Function GetStudentList() As Object
        Dim students As New List(Of Student)
        Try
            students = bbdd.GetListStudent()
        Catch ex As Exception
            Return Request.CreateResponse(HttpStatusCode.BadRequest, (New With {.status = 401, .message = "Error"}))
        End Try
        Return Json(New With {
            .students = students
                    })
    End Function

    <HttpPost>
    Function ExistUsername(data As Newtonsoft.Json.Linq.JObject) As Object
        Dim username As String = CType(data.ToObject(Of Dictionary(Of String, Object)).Item("username"), String)
        Dim exist As Boolean = False
        Try
            exist = bbdd.ExistUser(username)
        Catch ex As Exception
            Return Request.CreateResponse(HttpStatusCode.BadRequest, (New With {.status = 401, .message = "Error"}))
        End Try
        Return Json(New With {
            .exist = exist
                    })
    End Function

    <HttpPost>
    Function CreateStudent(data As Newtonsoft.Json.Linq.JObject) As Object
        Dim msg As String = String.Empty
        Dim err As Boolean = False
        Dim result = 0
        Dim student As String = CType(data.ToObject(Of Dictionary(Of String, Object)).Item("student"), String)
        Dim isNew As Boolean = CType(data.ToObject(Of Dictionary(Of String, Object)).Item("mode"), Boolean)
        Try
            Dim obj = JsonConvert.DeserializeObject(Of Student)(student)
            result = bbdd.Save(obj, isNew)
            If (result = 0) Then
                err = True
                msg = "Error saving object."
            End If
        Catch ex As Exception
            err = True
            msg = ex.ToString()
        End Try
        Return Json(New With {
                .error = err,
                .status = msg,
                .id = result
             })
    End Function

    <HttpPost>
    Function Delete(data As Newtonsoft.Json.Linq.JObject) As Object
        Dim msg As String = String.Empty
        Dim err As Boolean = False
        Dim result = 0
        Dim id As String = CType(data.ToObject(Of Dictionary(Of String, Object)).Item("id"), String)
        Try
            bbdd.Delete(id)
        Catch ex As Exception
            err = True
            msg = ex.ToString()
        End Try
        Return Json(New With {
                .error = err,
                .status = msg,
                .id = result
             })
    End Function

End Class
